<?php

namespace micro\controllers;

use yii\web\Controller;

class RestController extends Controller
{
    
    public $enableCsrfValidation = false;
    
    public function actionIndex()
    {
        return 'Hello World!';
    }
}